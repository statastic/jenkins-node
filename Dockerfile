FROM debian:bullseye-slim

ARG user=jenkins
ARG group=jenkins

# uid 7001 for experimenting with userid-mapping
ARG uid=7001
ARG gid=7001

RUN groupadd -g ${gid} ${group} \
    && useradd -d "/home/${user}" -u "${uid}" -g "${gid}" -m -s /bin/bash "${user}"

ARG AGENT_WORKDIR=/home/${user}/agent

# Java ist mounted as volume to allow
# the creation of nodes with different jdk versions.
ENV JAVA_HOME=/opt/java
ENV PATH=${JAVA_HOME}/bin:${PATH}

ADD files/statastic-rootCA.crt /tmp/statastic-rootCA.crt

RUN apt-get update \
&& apt-get install  --no-install-recommends -y ca-certificates git locales jq curl \
&& echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
&& locale-gen \
&& mv /tmp/statastic-rootCA.crt /usr/local/share/ca-certificates/ \
&& update-ca-certificates \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*

USER ${user}
ENV LC_ALL=en_US.UTF-8

ENV AGENT_WORKDIR=${AGENT_WORKDIR}
RUN mkdir /home/${user}/.jenkins && mkdir -p ${AGENT_WORKDIR}

RUN git config --global user.email jenkins@statastic.io && \
    git config --global user.name jenkins

VOLUME /home/${user}/.jenkins
VOLUME ${AGENT_WORKDIR}
VOLUME ${JAVA_HOME}
WORKDIR /home/${user}